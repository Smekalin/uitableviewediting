//
//  Student.m
//  TableViewEditing1
//
//  Created by Sergey on 13/09/16.
//  Copyright © 2016 Sergey. All rights reserved.
//

#import "Student.h"

@implementation Student
static NSString *firstNames[] = {
    @"firstName1"
    , @"firstName2"
    , @"firstName3"
    , @"firstName4"
    , @"firstName5"
    , @"firstName6"
    , @"firstName7"
    , @"firstName8"
    
};

static NSString *lastNames[] = {
    @"lastName1"
    , @"lastName2"
    , @"lastName3"
    , @"lastName4"
    , @"lastName5"
    , @"lastName6"
    , @"lastName7"
    , @"lastName8"
    
};

static int namesCount = 8;

+ (Student *)randomStudent {
    Student *student = [[Student alloc] init];
    student.firstName = firstNames[arc4random() % namesCount];
    student.lastName = lastNames[arc4random() % namesCount];
    student.averageGrade = (CGFloat)(200 + arc4random() % (500 - 200)) / 100.f;
    
    return student;
}
@end
