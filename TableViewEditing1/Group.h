//
//  Group.h
//  TableViewEditing1
//
//  Created by Sergey on 13/09/16.
//  Copyright © 2016 Sergey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Group : NSObject
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSArray *students;
@end
